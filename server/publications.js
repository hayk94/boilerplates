import { Todos } from '../imports/collections'

const todoPubFields = {
  text: 1,
  completed: 1
}

const getTodoPublication = function (filter, pageSkip = 0) {
  const queryObject = {}

  switch (filter) {
    case 'SHOW_COMPLETED':
      return query.completed = true
    case 'SHOW_ACTIVE':
      return query.completed = false
    default:
      break
  }

  Counts.publish(this, 'TodoCount', Todos.find(queryObject))
  return Todos.find(queryObject, {fields: todoPubFields, skip: pageSkip, limit: 10})
}

Meteor.publish('Todos', getTodoPublication)
