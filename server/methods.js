import { Todos } from '../imports/collections'

Meteor.methods({
  addTodo (text) {
    return Todos.insert({
      text,
      completed: false
    })
  },

  toggleTodo (_id) {
    const todoInQuestion = Todos.findOne(_id, {fields: {completed: 1}})
    const completed = todoInQuestion.completed
    return Todos.update({_id}, {$set: {completed: !completed}})
  }
})
