export default function toggleTodo (text) {
  return () => Meteor.call('toggleTodo', text)
}
