import React, { Component } from 'react'
import { createContainer } from 'react-meteor-data'
import { connect } from 'react-redux'
import { Todos } from '../../collections'
import changePage from '../actions/changePage'
import toggleTodo from '../actions/toggleTodo'
import Todo from './Todo'
import Pagination from './Pagination'

class TodoList extends Component {
  render () {
    const { dispatch } = this.props
    const todos = this.props.todoList
    const pagination = this.props.todoCount > 10 ? (
      <Pagination
        handlePageClick={({data}) => dispatch(changePage(data.selected))}
        pageCount={this.props.todoCount/10}
      />
    ) : ''
    return (
      <div>
        <ul>
          {
            todos.map(todo =>
              <Todo
                key={todo._id}
                {...todo}
                onClick={() => dispatch(toggleTodo(todo._id))}
              />
            )
          }
        </ul>
        {pagination}
      </div>
    )
  }
}

const TodoContainer = createContainer(({visibilityFilter, pageSkip}) => {
  const todoSub = Meteor.subscribe('Todos', visibilityFilter, pageSkip)
  return {
    todoSubReady: todoSub.ready(),
    todoList: Todos.find({}, {limit: 10}).fetch() || [],
    todoCount: Counts.get('TodoCount')
  }
}, TodoList)

function mapStateToProps ({visibilityFilter, pageSkip}) {
  return {
    visibilityFilter,
    pageSkip
  }
}

export default connect(mapStateToProps)(TodoContainer)
