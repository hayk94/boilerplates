import React from 'react'
import { connect } from 'react-redux'

import addTodo from '../actions/addTodo'

const AddTodo = ({dispatch}) => {
  let input = {}

  return (
    <div>
      <input ref={(elem) => input = elem} />
      <button onClick={() => {
        dispatch(addTodo(input.value))
      }}>
        Add Todo
      </button>
    </div>
  )
}

export default connect()(AddTodo)
