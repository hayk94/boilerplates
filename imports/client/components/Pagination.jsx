import React from 'react'
import ReactPaginate from 'react-paginate'

export default function Pagination ({ handlePageClick, pageCount }) {
  return (
    <div className="paginate">
      <ReactPaginate
        previousLable={'previous'}
        nextLabel={'nextLabel'}
        breakLabel={<li className="break"><a href="">...</a></li>}
        pageNum={pageCount}
        marginPageDisplayed={2}
        pageRangeDisplayed={5}
        clickCallback={handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName='pages pagination'
        activeClassName='active'
      />
    </div>
  )
}
