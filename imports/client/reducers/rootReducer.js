import { combineReducers } from 'redux'
import visibilityFilter from './visibilityReducer'
import pageSkip from './pageSkipReducer'

const rootReducer = combineReducers({
  visibilityFilter,
  pageSkip
})

export default rootReducer
